from django.apps import AppConfig


class TestworkAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'testwork_app'
