import datetime
from django import forms

from .models import Company, Sharer
from django.forms import ModelForm, TextInput, NumberInput, DateInput, ModelChoiceField


class CompanyForm(ModelForm):
    fk_sharer = ModelChoiceField(queryset=Sharer.objects.all(), widget=forms.Select(attrs={'class':'form-control'}), label='Osaniku nimi')

    class Meta:
        model = Company
        fields = ['company_name', 'company_code', 'reg_date', 'capital', 'fk_sharer', 'eur_share']
        widgets = {
            'company_name': TextInput(attrs={'class': 'form-control'}),
            'company_code': TextInput(attrs={'class': 'form-control'}),
            'reg_date': DateInput(attrs={'class': 'form-control'}),
            'capital': NumberInput(attrs={'class': 'form-control'}),
            'eur_share': NumberInput(attrs={'class': 'form-control'}),
        }

    def clean_company_name(self):
        company_name = self.cleaned_data.get('company_name')
        if len(company_name) < 3 or len(company_name) > 100:
            raise forms.ValidationError('Nimi peab olema 3 kuni 100 tähte või numbrit')
        else:
            return company_name

    def clean_company_code(self):
        company_code = self.cleaned_data.get('company_code', False)
        if len(company_code) != 7:
            raise forms.ValidationError('Kood peab olema 7 numbrit')
        else:
            return company_code

    def clean_reg_date(self):
        reg_date = self.cleaned_data.get('reg_date', False)
        if reg_date > datetime.date.today():
            raise forms.ValidationError('Asutamiskuupäev peab olema väiksem või võrdne käesoleva kuupäevaga')
        else:
            return reg_date

    def clean_capital(self):
        capital = self.cleaned_data.get('capital', False)
        if capital < 2500:
            raise forms.ValidationError('Kogukapitali suurus eurodes peab olema täisarv ja vähemalt 2500')
        else:
            return capital

    def clean_eur_share(self):
        eur_share = self.cleaned_data.get('eur_share', False)
        if eur_share < 1:
            raise forms.ValidationError('Osaniku osa suurus eurodes peab olema täisarv ja vähemalt 1')
        else:
            return eur_share










#
#
# class CompanyForm(forms.ModelForm):
#
#     fk_sharer = forms.ModelChoiceField(queryset=Sharer.objects.all(), label='Osanik')
#
#     # company_name = forms.CharField(label='Osaühingu nimi')
#     # company_code = forms.CharField(label='Osaühingu registrikood')
#     # reg_date = forms.DateField(initial=datetime.date.today, label='Asutamiskuupäev')
#     # capital = forms.CharField(label='Kogukapitali suurus eurodes')
#     # fk_sharer = forms.ForeignKey(Sharer, on_delete=models.CASCADE, default='', null=True)
#
#
    # def clean_company_name(self):
    #     data = self.cleaned_data['company_name']
    #
    #     if len(data) < 3 or len(data) > 100:
    #         raise ValidationError('Nimi peab olema 3 kuni 100 tähte või numbrit')
    #     else:
    #         return data
#
#     def clean_company_code(self):
#         data = self.cleaned_data['company_code']
#
#         if len(data) != 7 or (data.isdigit() == False):
#             raise ValidationError('Kood peab olema 7 numbrit')
#         else:
#             return data
#
#     def clean_reg_date(self):
#         data = self.cleaned_data['reg_date']
#
#         if data > datetime.date.today():
#             raise ValidationError('Asutamiskuupäev peab olema väiksem või võrdne käesoleva kuupäevaga')
#         else:
#             return data
#
#     def clean_capital(self):
#         data = self.cleaned_data['capital']
#
#         if data < 2500:
#             raise ValidationError('Kogukapitali suurus eurodes peab olema täisarv ja vähemalt 2500')
#         else:
#             return data
#
#     # def save(self, commit=True):
#     #     obj, created = Sharer.objects.get_or_create(
#     #         sharer_name=self.cleaned_data['fk_sharer']
#     #     )
#     #     self.cleaned_data['fk_sharer'] = obj.id
#     #     return super().save(commit)
#
#     class Meta:
#         model = Company
#         fields = ('company_name', 'company_code', 'reg_date', 'capital', 'fk_sharer')
#
#         widgets = {
#             'company_name': forms.TextInput(attrs={'class': 'form-control'}),
#             'company_code': forms.TextInput(attrs={'class': 'form-control'}),
#             'reg_date': forms.DateInput(attrs={'class': 'form-control'}),
#             'capital': forms.TextInput(attrs={'class': 'form-control'}),
#         }
#
#
# class SharerForm(forms.ModelForm):
#     # sharer_name = forms.CharField(label='Osaniku nimi')
#     # sharer_code = forms.CharField(label='Osaniku kood')
#     # eur_share = forms.CharField(label='Osaniku osa suurus eurodes')
#
#     def clean_eur_share(self):
#         data = self.cleaned_data['eur_share']
#
#         if data < 1:
#             raise ValidationError('Osaniku osa suurus eurodes peab olema täisarv ja vähemalt 1')
#         else:
#             return data
#
#     class Meta:
#         model = Sharer
#         fields = ('eur_share',)
#
#         widgets = {
#             # 'sharer_name': forms.TextInput(attrs={'class': 'form-control'}),
#             # 'sharer_code': forms.TextInput(attrs={'class': 'form-control'}),
#             'eur_share': forms.DateInput(attrs={'class': 'form-control'}),
#         }
#
#
#
#
# # class CompanyForm(forms.ModelForm):
# #
# #     class Meta:
# #         model = Company
# #         fields = ['company_name', 'company_code', 'reg_date', 'capital', 'fk_sharer']
# #
# # class SharerForm(forms.ModelForm):
# #
# #     class Meta:
# #         model = Sharer
# #         fields = ['sharer_name', 'sharer_code', 'eur_share']
#
#
#
