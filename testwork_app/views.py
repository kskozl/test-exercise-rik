import datetime

from django.contrib import messages
from django.http import HttpResponse
from django.core.exceptions import ValidationError

from django.shortcuts import render, redirect
from .forms import CompanyForm
from django.views.generic import TemplateView, ListView
from django.db.models import Q

from .models import Company, Sharer
from django.views.generic import DetailView

def home(request):
    # Avaleht + Osaühingute otsing;
    companies = Company.objects.all()
    return render(request, 'home.html', {"companies": companies})

def add(request):
    # Osaühingu asutamine;
    error = ''
    if request.method == 'POST':
        company_form = CompanyForm(request.POST)
        if company_form.is_valid():
            company_form.save()
            return redirect('/')
        else:
            error = 'Andmed on valed. Kontrollige palun sisestatud andmeid.'
    company_form = CompanyForm(request.POST)
    data = {
        'error': error,
        'company_form': company_form,
    }
    return render(request, 'add.html', data)

# def search(request):
#     results = []
#     if request.method == "GET":
#         query = request.GET.get('search')
#         results = Company.objects.filter(Q(company_name__icontains=query) | Q(company_code__icontains=query))
#
#         return render(request, 'search.html', {'query': query, 'results': results})

class CompanyDetailView(DetailView):
    # Osaühingu andmete vaatamine.
    model = Company
    template_name = 'overview.html'
    context_object_name = 'company'


# class SearchResultsView(ListView):
#     model = Company
#     template_name = 'search.html'
#
#     def get_queryset(self):
#         query = self.request.GET.get('search')
#         object_list = Company.objects.filter(Q(company_name__icontains=query) | Q(company_code__icontains=query))
#         return object_list


# def search(request):
#
#     if request.method == "GET":
#
#     return HttpResponse("home.html")


# def view(request, pk):
#     # Osaühingu andmete vaatamine.
#     company = Company.objects.all()
#     sharer = Sharer.objects.all()
#     return HttpResponse({pk})
    # return render(request, 'overview.html', {"company": company, "sharer": sharer})

    # if request.method == 'POST':
    #     form = CompanyForm(request.POST)
    #     form2 = SharerForm(request.POST)
    #     if form.is_valid():
    #         # cd = form.cleaned_data
    #         # cd2 = form2.cleaned_data
    #         # company = Company()
    #         # sharer = Sharer()
    #         form.save()
    #         form2.save()
    #         return redirect("overview.html")
    # form = CompanyForm()
    # form2 = SharerForm()
    # return render(request=request, template_name="add.html", context={"company_form": form, "sharer_form": form2})


# def addSharer(request):
#     if request.method == 'POST':
#         shr_form = SharerForm(data=request.POST)
#         if shr_form.is_valid():
#             shr_form.save()
#             return redirect("/")
#     shr_form = SharerForm()
#     return render(request=request, template_name="add.html", context={"sharer_form": shr_form})







# class SharerDetailView(DetailView):
#     # Osaniku andmete vaatamine.
#     model = Sharer
#     template_name = 'overview.html'
#     context_object_name = 'sharer'
