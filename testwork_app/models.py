import datetime
from django.db import models


class Sharer(models.Model):

    sharer_name = models.CharField(null=True, blank=True, max_length=100, verbose_name='Osaniku nimi')
    sharer_code = models.CharField(null=True, blank=True, max_length=13, verbose_name='Osaniku kood')

    def __str__(self):
        return self.sharer_name

class Company(models.Model):

    company_name = models.CharField(max_length=100, verbose_name='Osaühingu nimi')
    company_code = models.CharField(max_length=7, verbose_name='Osaühingu registrikood')
    reg_date = models.DateField(verbose_name='Asutamiskuupäev', default=datetime.date.today)
    capital = models.IntegerField(verbose_name='Kogukapitali suurus eurodes')

    fk_sharer = models.ForeignKey(Sharer, on_delete=models.CASCADE, null=True, blank=True)
    eur_share = models.IntegerField(blank=True, null=True, verbose_name='Osaniku osa suurus eurodes')

    def __str__(self):
        return self.company_name


