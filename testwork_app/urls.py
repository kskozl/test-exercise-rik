from django.contrib import admin
from django.urls import path
from . import views
# from .views import SearchResultsView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name='home'),
    path('add/', views.add),
    path('<int:pk>', views.CompanyDetailView.as_view(), name='view'),
    # path('search/', views.search, name='search'),

]
