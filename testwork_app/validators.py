# import datetime
# from django.core.exceptions import ValidationError
#
#
# def clean_company_name(self):
#     data = self.cleaned_data['company_name']
#
#     if len(data) < 3 or len(data) > 100:
#         raise ValidationError('Nimi peab olema 3 kuni 100 tähte või numbrit')
#     else:
#         return data
#
#
# def clean_company_code(self):
#     data = self.cleaned_data['company_code']
#
#     if len(data) != 7 or (data.isdigit() == False):
#         raise ValidationError('Kood peab olema 7 numbrit')
#     else:
#         return data
#
#
# def clean_reg_date(self):
#     data = self.cleaned_data['reg_date']
#
#     if data > datetime.date.today():
#         raise ValidationError('Asutamiskuupäev peab olema väiksem või võrdne käesoleva kuupäevaga')
#     else:
#         return data
#
#
# def clean_capital(self):
#     data = self.cleaned_data['capital']
#
#     if (data.isdigit() == False) or data < 2500:
#         raise ValidationError('Kogukapitali suurus eurodes peab olema täisarv ja vähemalt 2500')
#     else:
#         return data
#
# def clean_eur_share(self):
#     data = self.cleaned_data['eur_share']
#
#     if (data.isdigit() == False) or data < 1:
#         raise ValidationError('Osaniku osa suurus eurodes peab olema täisarv ja vähemalt 1')
#     else:
#         return data
