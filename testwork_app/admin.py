from django.contrib import admin
from .models import Company, Sharer

admin.site.register(Company)
admin.site.register(Sharer)




# insert into testwork_app_sharer (id, sharer_name, sharer_code) values ('1', 'Test Osanik 1','49512050865');
# insert into testwork_app_sharer (id, sharer_name, sharer_code) values ('2', 'Test Osanik 2','38911010996');

# insert into testwork_app_company (id, company_name, company_code, reg_date, capital, fk_sharer_id, eur_share) values ('1', 'Test Osaühing 1','1234567', '2022-06-01', '3000', '1', '550');
# insert into testwork_app_company (id, company_name, company_code, reg_date, capital, fk_sharer_id, eur_share) values ('2', 'Test Osaühing 2','7654321', '2022-05-01', '4000', '2', '600');




